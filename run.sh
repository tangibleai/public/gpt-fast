# https://github.com/pytorch-labs/gpt-fast.git
# supported models:
#   openlm-research/open_llama_7b
#   meta-llama/Llama-2-7b-chat-hf
#   meta-llama/Llama-2-13b-chat-hf
#   meta-llama/Llama-2-70b-chat-hf
#   codellama/CodeLlama-7b-Python-hf
#   codellama/CodeLlama-34b-Python-hf

# HF_TOKEN:
source .env

prompt="'$*'"
export MODEL_REPO=meta-llama/Llama-2-13b-chat-hf
if [ ! -f "checkpoints/$MODEL_REPO/model.pth" ]; then
    # ./scripts/prepare.sh $MODEL_REPO 
    python scripts/download.py --hf_token=$HF_TOKEN --repo_id=$MODEL_REPO \
        && python scripts/convert_hf_checkpoint.py --checkpoint_dir checkpoints/$MODEL_REPO \
        && python quantize.py --checkpoint_path checkpoints/$MODEL_REPO/model.pth --mode int8
fi

if [ -z "$prompt" ] ; then
    prompt="You awake from a nightmare in a damp dark room lying on a bare cot. Less than emphatic light is leaking in around the shutters over a small square window on the far side of the "
fi

python generate.py --compile --checkpoint_path checkpoints/$MODEL_REPO/model.pth --prompt "$prompt"
